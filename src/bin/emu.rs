use clap::Parser as _;

#[derive(clap::Parser, Debug)]
#[command(author, version, about)]
struct Opt {
    #[command(subcommand)]
    emulator: Emulators,
}

#[derive(clap::Subcommand, Debug)]
enum Emulators {
    Chip8(emu::engines::chip8::Opt),
}

fn main() {
    let opt = Opt::parse();
    println!("{:?}", opt);
    match opt.emulator {
        Emulators::Chip8(opt) => emu::engines::chip8::execute(opt),
    }
}
