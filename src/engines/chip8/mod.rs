#![allow(dead_code)]
#![deny(missing_docs)]

//! Chip8
//!
//! This is a full interpreter implementation for the
//! chip8.  It does not currently implement any revisions
//! or alternative implementations.

use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

use rand::{rngs::StdRng, Rng, RngCore, SeedableRng};
use sdl2::{
    audio::{AudioCallback, AudioSpecDesired, AudioStatus},
    event::Event,
    keyboard::Scancode,
    pixels::{Color, PixelFormatEnum},
};

mod constants;
use constants::*;

/// blah
#[derive(clap::Parser, Debug)]
pub struct Opt {
    #[arg(name = "ROM")]
    rom: PathBuf,
}

/// Result type for this library
pub type Result<T> = core::result::Result<T, Error>;

/// Errors this library can return
#[derive(Debug)]
pub enum Error {
    /// The ROM is too large
    RomSize(usize),
    /// Encountered an Invalid or Unimplemented Opcode
    InvalidOpCode(u8, u8),
}

/// This contains the state of the interpreter.
#[derive(Debug)]
pub struct Cpu<R: RngCore> {
    memory: [u8; MEM_SIZE],                           // 4 KB of memory
    v: [u8; 16],                                      // 16 general purpose u8 registers
    i: u16,                   // Memory pointer, u16 but generally used as a u12
    dt: u8,                   // Display timer
    st: u8,                   // Sound timer
    pc: u16,                  // Program counter
    sp: usize,                // Stack pointer
    stack: [u16; STACK_SIZE], // stack for storing pc on jump
    keypad: Keypad,           // 16 keys
    screen: [u8; (SCREEN_WIDTH * SCREEN_HEIGHT) / 8], // Each pixel is one bit
    r: R,
    tick: u8,
    rate: u32,
}

impl<R: RngCore> Cpu<R> {
    /// Create a new CPU using the specified Random generator
    fn with_rng(rand: R) -> Self {
        let mut memory = [0; MEM_SIZE];
        memory[0..80].copy_from_slice(&FONT);

        Self {
            memory,
            v: [0; 16],
            i: 0,
            dt: 0,
            st: 0,
            pc: 0,
            sp: 0,
            stack: [0; STACK_SIZE],
            keypad: Keypad::default(),
            screen: [0; (SCREEN_WIDTH * SCREEN_HEIGHT) / 8],
            r: rand,
            tick: 0,
            rate: 400,
        }
    }

    /// Set the rate the game should run at, defaults to 400Hz
    pub fn set_rate(mut self, rate: u32) -> Self {
        self.rate = rate;
        self
    }
}

impl Cpu<StdRng> {
    /// Create a new CPU with default settings
    pub fn new() -> Self {
        let rng = StdRng::from_entropy();
        Self::with_rng(rng)
    }
}

impl Default for Cpu<StdRng> {
    fn default() -> Self {
        Self::new()
    }
}

impl<R: RngCore> Cpu<R> {
    /// 00E0 - Clear screen
    fn op_00e0(&mut self) {
        self.screen = [0; (SCREEN_WIDTH * SCREEN_HEIGHT) / 8];
        self.pc += 2;
    }

    /// 00EE - Return from function
    fn op_00ee(&mut self) {
        self.sp -= 1;
        self.pc = self.stack[self.sp];
        self.pc += 2;
    }

    /// 1nnn - Jump to address
    fn op_1nnn(&mut self, pc: u16) {
        self.pc = pc;
    }

    /// 2nnn - Call function
    fn op_2nnn(&mut self, pc: u16) {
        self.stack[self.sp] = self.pc;
        self.sp += 1;
        self.pc = pc;
    }

    /// 3xkk - Skip if Vx == kk
    fn op_3xkk(&mut self, register: u8, value: u8) {
        if self.v[register as usize] == value {
            self.pc += 2;
        }
        self.pc += 2;
    }

    /// 4xkk - Skip if Vx != kk
    fn op_4xkk(&mut self, register: u8, value: u8) {
        if self.v[register as usize] != value {
            self.pc += 2;
        }
        self.pc += 2;
    }

    /// 5xy0 - Skip if Vx == Vy
    fn op_5xy0(&mut self, registerx: u8, registery: u8) {
        if self.v[registerx as usize] == self.v[registery as usize] {
            self.pc += 2;
        }
        self.pc += 2;
    }

    /// 6xkk - Vx := kk
    fn op_6xkk(&mut self, register: u8, value: u8) {
        self.v[register as usize] = value;
        self.pc += 2;
    }

    /// 7xkk - Vx := Vx + kk
    fn op_7xkk(&mut self, register: u8, value: u8) {
        let (v, _c) = self.v[register as usize].overflowing_add(value);
        self.v[register as usize] = v;
        self.pc += 2;
    }

    /// 8xy0 - Vx := Vy
    fn op_8xy0(&mut self, registerx: u8, registery: u8) {
        self.v[registerx as usize] = self.v[registery as usize];
        self.pc += 2;
    }

    /// 8xy1 - Vx := Vx | Vy
    fn op_8xy1(&mut self, registerx: u8, registery: u8) {
        self.v[registerx as usize] |= self.v[registery as usize];
        self.pc += 2;
    }

    /// 8xy2 - Vx := Vx & Vy
    fn op_8xy2(&mut self, registerx: u8, registery: u8) {
        self.v[registerx as usize] &= self.v[registery as usize];
        self.pc += 2;
    }

    /// 8xy3 - Vx := Vx ^ Vy
    fn op_8xy3(&mut self, registerx: u8, registery: u8) {
        self.v[registerx as usize] ^= self.v[registery as usize];
        self.pc += 2;
    }

    /// 8xy4 - Vx := Vx + Vy, VF = carry
    fn op_8xy4(&mut self, registerx: u8, registery: u8) {
        let (v, c) = self.v[registerx as usize].overflowing_add(self.v[registery as usize]);
        self.v[registerx as usize] = v;
        self.v[0xF] = if c { 1 } else { 0 };
        self.pc += 2;
    }

    /// 8xy5 - Vx := Vx - Vy, VF := NOT BORROW
    fn op_8xy5(&mut self, registerx: u8, registery: u8) {
        let (v, c) = self.v[registerx as usize].overflowing_sub(self.v[registery as usize]);
        self.v[registerx as usize] = v;
        self.v[0xF] = if c { 0 } else { 1 };
        self.pc += 2;
    }

    /// 8xy6 - VF := LSB(Vx), Vx := Vx >> 1
    fn op_8xy6(&mut self, registerx: u8, registery: u8) {
        let v = self.v[registery as usize];
        let (v, c) = v.overflowing_shr(1);
        self.v[0xF] = if c { 1 } else { 0 };
        self.v[registerx as usize] = v;
        self.pc += 2;
    }

    /// 8xy7 - Vx := Vy - Vx, VF := NOT BORROW
    fn op_8xy7(&mut self, registerx: u8, registery: u8) {
        let (v, c) = self.v[registery as usize].overflowing_sub(self.v[registerx as usize]);
        self.v[registerx as usize] = v;
        self.v[0xF] = if c { 0 } else { 1 };
        self.pc += 2;
    }

    /// 8xyE - VF := MSB(Vx), Vx := Vx << 1
    fn op_8xye(&mut self, registerx: u8, registery: u8) {
        let v = self.v[registery as usize];
        let (v, c) = v.overflowing_shl(1);
        self.v[0xF] = if c { 1 } else { 0 };
        self.v[registerx as usize] = v;
        self.pc += 2;
    }

    /// 9xy0 - Skip if Vx != Vy
    fn op_9xy0(&mut self, registerx: u8, registery: u8) {
        if self.v[registerx as usize] != self.v[registery as usize] {
            self.pc += 2;
        }
        self.pc += 2;
    }

    /// Annn - I := nnn
    fn op_annn(&mut self, value: u16) {
        self.i = value;
        self.pc += 2;
    }

    /// Bnnn - Jump nnn + V0
    fn op_bnnn(&mut self, value: u16) {
        self.pc = value + self.v[0] as u16;
    }

    /// Cxkk - Vx := rand & kk
    fn op_cxkk(&mut self, register: u8, value: u8) {
        self.v[register as usize] = self.r.gen::<u8>() & value;
        self.pc += 2;
    }

    /// Dxyn - Display n byte sprite from I starting at (Vx, Vy)
    fn op_dxyn(&mut self, registerx: u8, registery: u8, size: u8) {
        let mut colision = false;
        let hoffset = self.v[registerx as usize] % 8;
        for voffset in 0..size {
            let ypos = (self.v[registery as usize] + voffset) as usize % SCREEN_HEIGHT;
            let xpos = (self.v[registerx as usize] as usize % SCREEN_WIDTH) / 8;
            let pos = ypos * 8 + xpos;
            let byte = self.memory[(self.i + voffset as u16) as usize];
            colision = colision || (self.screen[pos] & (byte >> hoffset)) != 0;
            self.screen[pos] ^= byte >> hoffset;

            if hoffset != 0 {
                let xpos = (xpos + 1) % SCREEN_WIDTH;
                let pos = ypos * 8 + xpos;
                let byte = self.memory[(self.i + voffset as u16) as usize];
                colision = colision || (self.screen[pos] & (byte << (8 - hoffset))) != 0;
                self.screen[pos] ^= byte << (8 - hoffset);
            }
        }
        if colision {
            self.v[15] = 1;
        } else {
            self.v[15] = 0;
        }
        self.pc += 2;
    }

    /// Ex9E - Skip if key of value Vx is pressed
    fn op_ex9e(&mut self, register: u8) {
        if self.keypad.is_pressed(self.v[register as usize]) {
            self.pc += 2;
        }
        self.pc += 2;
    }

    /// ExA1 - Skip if key of value Vx is not pressed
    fn op_exa1(&mut self, register: u8) {
        if !self.keypad.is_pressed(self.v[register as usize]) {
            self.pc += 2;
        }
        self.pc += 2;
    }

    /// Fx07 - Vx := DT
    fn op_fx07(&mut self, register: u8) {
        self.v[register as usize] = self.dt;
        self.pc += 2;
    }

    /// Fx0A - Wait for keypress and store in Vx
    ///
    /// Rather than actually blocking we check if a key is pressed and only move to the next
    /// instruction if one is pressed.
    fn op_fx0a(&mut self, register: u8) {
        if self.keypad.is_any_pressed() {
            self.v[register as usize] = self.keypad.get_first_pressed().unwrap_or(0);
            self.pc += 2;
        }
    }

    /// Fx15 - DT := Vx
    fn op_fx15(&mut self, register: u8) {
        self.dt = self.v[register as usize];
        self.pc += 2;
    }

    /// Fx18 - ST := Vx
    fn op_fx18(&mut self, register: u8) {
        self.st = self.v[register as usize];
        self.pc += 2;
    }

    /// Fx1E - I := I + Vx
    fn op_fx1e(&mut self, register: u8) {
        self.i += self.v[register as usize] as u16;
        self.pc += 2;
    }

    /// Fx29 - Set I to location of digit sprite Vx
    fn op_fx29(&mut self, register: u8) {
        self.i = self.v[register as usize] as u16 * 5;
        self.pc += 2;
    }

    /// Fx33 - Store Hundreds/Tens/Ones into I, I+1, I+2
    fn op_fx33(&mut self, register: u8) {
        let val = self.v[register as usize];
        self.memory[self.i as usize] = val / 100;
        self.memory[(self.i + 1) as usize] = (val / 10) % 10;
        self.memory[(self.i + 2) as usize] = val % 10;
        self.pc += 2;
    }

    /// Fx55 - Store V0-Vx starting at I
    fn op_fx55(&mut self, register: u8) {
        for each in 0..=register {
            self.memory[(self.i + each as u16) as usize] = self.v[each as usize];
        }
        self.pc += 2;
    }

    /// Fx65 - Load V0-Vx starting at I
    fn op_fx65(&mut self, register: u8) {
        for each in 0..=register {
            self.v[each as usize] = self.memory[(self.i + each as u16) as usize];
        }
        self.pc += 2;
    }

    /// Load the rom into the interpreters memory space
    pub fn load_rom(&mut self, rom: &[u8]) -> Result<()> {
        if rom.len() > 4096 - 512 {
            Err(Error::RomSize(rom.len()))
        } else {
            self.memory[512..(512 + rom.len())].copy_from_slice(rom);
            self.pc = 512;
            Ok(())
        }
    }

    /// Get the screen buffer
    ///
    /// The screen is stored as an array which should be interpreted as a 64x32 bit screen.
    /// The array should be read as if scanning left-to-right, top-down and each byte represents 8
    /// pixels.  This means every 4 bytes is one scan line.
    pub fn get_screen_buffer(&self) -> &[u8; (SCREEN_WIDTH * SCREEN_HEIGHT) / 8] {
        &self.screen
    }

    /// Get a mutable reference to the screen buffer
    ///
    /// This is the same buffer provided by 'get_screen_buffer' but mutable.
    pub fn get_screen_buffer_mut(&mut self) -> &mut [u8; (SCREEN_WIDTH * SCREEN_HEIGHT) / 8] {
        &mut self.screen
    }

    /// Get the sound timer current value
    pub fn get_sound_timer(&self) -> u8 {
        self.st
    }

    /// Simulate one instruction executed on the interpreter
    pub fn tick(&mut self, keypad: Keypad) -> Result<()> {
        self.keypad = keypad;
        let i0 = self.memory[self.pc as usize];
        let i1 = self.memory[self.pc as usize + 1];

        match i0 & 0xf0 {
            0x00 => match i1 {
                0xE0 => self.op_00e0(),
                0xEE => self.op_00ee(),
                _ => return Err(Error::InvalidOpCode(i0, i1)),
            },
            0x10 => self.op_1nnn((((i0 & 0x0F) as u16) << 8) + i1 as u16),
            0x20 => self.op_2nnn((((i0 & 0x0F) as u16) << 8) + i1 as u16),
            0x30 => self.op_3xkk(i0 & 0x0F, i1),
            0x40 => self.op_4xkk(i0 & 0x0F, i1),
            0x50 => self.op_5xy0(i0 & 0x0F, (i1 & 0xF0) >> 4),
            0x60 => self.op_6xkk(i0 & 0x0F, i1),
            0x70 => self.op_7xkk(i0 & 0x0F, i1),
            0x80 => match i1 & 0x0F {
                0x0 => self.op_8xy0(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0x1 => self.op_8xy1(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0x2 => self.op_8xy2(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0x3 => self.op_8xy3(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0x4 => self.op_8xy4(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0x5 => self.op_8xy5(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0x6 => self.op_8xy6(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0x7 => self.op_8xy7(i0 & 0x0F, (i1 & 0xF0) >> 4),
                0xE => self.op_8xye(i0 & 0x0F, (i1 & 0xF0) << 4),
                _ => return Err(Error::InvalidOpCode(i0, i1)),
            },
            0x90 => self.op_9xy0(i0 & 0x0F, (i1 & 0xF0) >> 4),
            0xA0 => self.op_annn((((i0 & 0x0F) as u16) << 8) + i1 as u16),
            0xB0 => self.op_bnnn((((i0 & 0x0F) as u16) << 8) + i1 as u16),
            0xC0 => self.op_cxkk(i0 & 0x0F, i1),
            0xD0 => self.op_dxyn(i0 & 0x0F, (i1 & 0xF0) >> 4, i1 & 0x0F),
            0xE0 => match i1 {
                0x9E => self.op_ex9e(i0 & 0x0F),
                0xA1 => self.op_exa1(i0 & 0x0F),
                _ => return Err(Error::InvalidOpCode(i0, i1)),
            },
            0xF0 => match i1 {
                0x07 => self.op_fx07(i0 & 0x0F),
                0x0A => self.op_fx0a(i0 & 0x0F),
                0x15 => self.op_fx15(i0 & 0x0F),
                0x18 => self.op_fx18(i0 & 0x0F),
                0x1E => self.op_fx1e(i0 & 0x0F),
                0x29 => self.op_fx29(i0 & 0x0F),
                0x33 => self.op_fx33(i0 & 0x0F),
                0x55 => self.op_fx55(i0 & 0x0F),
                0x65 => self.op_fx65(i0 & 0x0F),
                _ => return Err(Error::InvalidOpCode(i0, i1)),
            },
            _ => return Err(Error::InvalidOpCode(i0, i1)),
        }

        self.tick += 1;
        if self.tick >= (self.rate as f64 / 60f64).round() as u8 {
            if self.dt > 0 {
                self.dt -= 1;
            }
            if self.st > 0 {
                self.st -= 1;
            }
            self.tick %= (self.rate as f64 / 60f64).round() as u8;
        }

        std::thread::sleep(std::time::Duration::from_secs(1) / self.rate);
        Ok(())
    }
}

/// This represents the keypad.
///
/// This struct needs to be used for the user to
/// interact with the interpreter.
#[derive(Debug, Default, Copy, Clone)]
pub struct Keypad {
    keypad: u16,
}

impl Keypad {
    /// Press a specific key
    pub fn press(&mut self, key: u8) {
        self.keypad |= 0x1 << key;
    }

    /// Unpress a specific key
    pub fn unpress(&mut self, key: u8) {
        self.keypad &= !(0x1 << key);
    }

    /// Set the current keys.
    ///
    /// The map is a simple bitmap where the LSB maps to
    /// key 1, and MSB maps to 16.
    fn set(&mut self, map: u16) {
        self.keypad = map;
    }

    /// Check if a particular key is currently pressed
    fn is_pressed(&self, key: u8) -> bool {
        (self.keypad & (0x1 << key)) != 0
    }

    /// Check if any keys are pressed
    fn is_any_pressed(&self) -> bool {
        self.keypad != 0
    }

    /// Get a keypress, the first key found is returned
    /// even if multiple keys are pressed.
    fn get_first_pressed(&self) -> Option<u8> {
        (0..15).find(|&bit| (self.keypad & (0x1 << bit)) != 0)
    }
}

struct SquareWave {
    phase_inc: f32,
    phase: f32,
    volume: f32,
}

impl AudioCallback for SquareWave {
    type Channel = f32;

    fn callback(&mut self, out: &mut [Self::Channel]) {
        for x in out.iter_mut() {
            *x = if self.phase <= 0.5 {
                self.volume
            } else {
                -self.volume
            };
            self.phase = (self.phase + self.phase_inc) % 1.0;
        }
    }
}

/// Executes
pub fn execute(opt: Opt) {
    let mut cpu = Cpu::default().set_rate(400);
    let mut rom = File::open(opt.rom).unwrap();
    let mut data = Vec::new();

    rom.read_to_end(&mut data).unwrap();
    cpu.load_rom(&data).unwrap();

    let sdl_context = sdl2::init().unwrap();
    let video_subsystem = sdl_context.video().unwrap();

    let window = video_subsystem
        .window("chip8", 64 * 16, 32 * 16)
        .position_centered()
        .build()
        .unwrap();

    let mut canvas = window
        .into_canvas()
        .present_vsync()
        .accelerated()
        .build()
        .unwrap();
    canvas.set_draw_color(Color::RGB(0, 0, 0));
    canvas.clear();
    canvas.present();

    // create_texture_streaming lets us get a raw buffer with `with_lock`
    // and manually write pixels into our texture.
    let texture_creator = canvas.texture_creator();
    let mut texture = texture_creator
        .create_texture_streaming(PixelFormatEnum::RGB24, 64, 32)
        .unwrap();

    let mut event_pump = sdl_context.event_pump().unwrap();

    let audio_subsystem = sdl_context.audio().unwrap();
    let desired_spec = AudioSpecDesired {
        freq: Some(44100), // Samples per second
        channels: Some(1), // Number of channels (mono)
        samples: None,     // Audio buffer size (use fallback)
    };
    let device = audio_subsystem
        .open_playback(None, &desired_spec, |spec| SquareWave {
            phase_inc: 440f32 / spec.freq as f32,
            phase: 0f32,
            volume: 0.25,
        })
        .unwrap();

    let mut keypad = Keypad::default();
    'running: loop {
        for event in event_pump.poll_iter() {
            match event {
                Event::Quit { .. }
                | Event::KeyDown {
                    scancode: Some(Scancode::Escape),
                    ..
                } => break 'running,
                Event::KeyDown {
                    scancode: Some(key),
                    ..
                } => match key {
                    Scancode::Num0 => (keypad).press(0),
                    Scancode::Num1 => (keypad).press(1),
                    Scancode::Num2 => (keypad).press(2),
                    Scancode::Num3 => (keypad).press(3),
                    Scancode::Num4 => (keypad).press(4),
                    Scancode::Num5 => (keypad).press(5),
                    Scancode::Num6 => (keypad).press(6),
                    Scancode::Num7 => (keypad).press(7),
                    Scancode::Num8 => (keypad).press(8),
                    Scancode::Num9 => (keypad).press(9),
                    Scancode::A => (keypad).press(10),
                    Scancode::B => (keypad).press(11),
                    Scancode::C => (keypad).press(12),
                    Scancode::D => (keypad).press(13),
                    Scancode::E => (keypad).press(14),
                    Scancode::F => (keypad).press(15),
                    _ => (),
                },
                Event::KeyUp {
                    scancode: Some(key),
                    ..
                } => match key {
                    Scancode::Num0 => (keypad).unpress(0),
                    Scancode::Num1 => (keypad).unpress(1),
                    Scancode::Num2 => (keypad).unpress(2),
                    Scancode::Num3 => (keypad).unpress(3),
                    Scancode::Num4 => (keypad).unpress(4),
                    Scancode::Num5 => (keypad).unpress(5),
                    Scancode::Num6 => (keypad).unpress(6),
                    Scancode::Num7 => (keypad).unpress(7),
                    Scancode::Num8 => (keypad).unpress(8),
                    Scancode::Num9 => (keypad).unpress(9),
                    Scancode::A => (keypad).unpress(10),
                    Scancode::B => (keypad).unpress(11),
                    Scancode::C => (keypad).unpress(12),
                    Scancode::D => (keypad).unpress(13),
                    Scancode::E => (keypad).unpress(14),
                    Scancode::F => (keypad).unpress(15),
                    _ => (),
                },
                _ => {}
            }
        }
        cpu.tick(keypad).unwrap();

        match (cpu.get_sound_timer(), device.status()) {
            (0, AudioStatus::Playing) => device.pause(),
            (x, AudioStatus::Paused) if x > 0 => device.resume(),
            (_, _) => {}
        };

        let screen = cpu.get_screen_buffer();
        texture
            .with_lock(None, |buf: &mut [u8], _pitch: usize| {
                let mut offset = 0;
                for byte in screen {
                    for shift in 0..=7 {
                        let (byte, _c) = byte.overflowing_shr(7 - shift);
                        if (byte & 0x1) != 0 {
                            buf[offset] = 255;
                            buf[offset + 1] = 255;
                            buf[offset + 2] = 255;
                        } else {
                            buf[offset] = 0;
                            buf[offset + 1] = 0;
                            buf[offset + 2] = 0;
                        }
                        offset += 3;
                    }
                }
            })
            .unwrap();
        canvas.clear();
        canvas.copy(&texture, None, None).unwrap();
        canvas.present();
    }
}
